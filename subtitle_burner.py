import subprocess
import os

os.chdir('videos/')

def readFilenames(): 
    files = set()
    with os.scandir('.') as entries:
        for entry in entries:
            filename = entry.name.split('.')[0]
            files.add(filename)
    return files

# Each filename must be unique to avoid processing the same file twice
files = readFilenames()



# Read all files and strip the extension 
# We use just the filename title to avoid having to if-else about the extensions
with os.scandir('.') as entries:
    for entry in entries:
        filename = entry.name.split('.')[0]
        files.add(filename)

# Rename all files to strip whitespace to prepare for further processing
def files_strip_whitespace(files):
    for file in files:
        filename = file.split(' ')
        filename = '_'.join(filename)
        os.rename(f'{file}.mp4', f'{filename}.mp4')
        os.rename(f'{file}.el.vtt', f'{filename}.el.vtt')

# Converts all subtitles to .ass
def convert_subtitles(filenames):
    for filename in filenames:
        command = ['ffmpeg',  '-i', f'{filename}.el.vtt', f'{filename}.ass']
        # We use subprocess call which is blocking and as such waits until the callee completes
        subprocess.call(command)

# Hardcodes the subtitles in the video file
def burnsubs_in_video(files):
    for filename in files:
        command = f'ffmpeg -i {filename}.mp4 -vf ass={filename}.ass burned_{filename}.mp4'
        command = command.split(' ')
        print(' '.join(command))
        subprocess.call(command)


files_strip_whitespace(files)

# Reread the files now that they were renamed
files = readFilenames()

convert_subtitles(files)
burnsubs_in_video(files)