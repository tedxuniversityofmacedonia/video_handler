# Usage

Tested with Python 3.6.7

## Download videos

Install the requirements by running `pip install -r requirements.txt`

Then add the video links in `links.txt` (example file included)

Finally just run `python3 download_videos.py`

## Hardcode subtitles in videos

Make sure both videos and subtitles where downloaded.

Then run `python3 subtitles_burner.py`